import pandas as pd


def get_titatic_dataframe() -> pd.DataFrame:
    df = pd.read_csv("train.csv")
    return df


def get_filled():
    df = get_titatic_dataframe()
    '''
    Put here a code for filling missing values in titanic dataset for column 'Age' and return these values in this view - [('Mr.', x, y), ('Mrs.', k, m), ('Miss.', l, n)]
    '''
    titles = ["Mr.", "Mrs.", "Miss."]
    
    df['Title'] = df['Name'].str.extract(' ([A-Za-z]+\.).*', expand=False)
    
    #print (df)
    
    results = []

    for title in titles:
        median_value = df.loc[df['Title'] == title, 'Age'].median()

        missing_values = df.loc[(df['Title'] == title) & (df['Age'].isnull())].shape[0]

        results.append((title, missing_values, median_value))

    return results

